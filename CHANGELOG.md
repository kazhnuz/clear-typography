# CHANGELOG

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [3.0.0] - 2024-10-05

- Rebrand du projet en moineau.css
- Simplification du code pour le rendre plus léger
- Ajout de la customisation de couleurs
- Support du mode sombre
- Support des éléments HTML suivants :
  - Balise `<details>`
  - Support des éléments de formulaires
  - Support des hgroups
  - Support des tableaux
- Ajout de classes utilitaires
  - `.flex` afin d'aligner simplement horizontalement des éléments
- Ajout d'une navbar avec un outil pour séparer en bloc

## [2.0.0] - 2019-11-07

### Added

- Modifiable variables for sizing

- Restore debug grid drawing

### Changed

- Switch to using rem instead of em for sizing
  
- Support vertical rythm
  
- Auto-calculate lineheight for titles

- Default lineheight is now 1.75

### Fixed

- Line-height is no longer affected by sup and sub

- Better overflowing for wells

## [1.2.0] - 2018-15-03

### Changed
  
- Switch to Open Sans, in order to solve problem with LightItalic font

## [1.1.0] - 2018-12-03

### Added

- Debug class added to show line heights

### Changed

- Stylesheet ported to SCSS
  
- Better typography : by making sure that lines always follow line heights

## [1.0.0] - 2018-10-03

### Added

- More markup support : pre, mark and blockquote

- Wrapper system to handle font size changes with responsive design

- Night Mode support

### Changed

- Better typographic support :

  - Don't justify text
  
  - Better contrast with text
  
  - Remove underlining and centering in title
  
- Tweaks in titles size

- Nicer styling for hr

## [0.1.0] - 2014-12-08

- Just a basic bad typographic css stylesheet.