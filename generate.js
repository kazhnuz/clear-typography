var marked = require('marked')
var fs = require('fs')

const inFile = "README.md"
const outFile = "dist/index.html"

function parse(inFile, outFile) {
    const inputContent = fs.readFileSync(inFile, 'utf8')

    const content = `<!DOCTYPE html>
    <html>
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <title>moineau.css</title>
            <link href="css/moineau.min.css" rel="stylesheet">
            <link
            rel="icon"
            href="data:image/svg+xml,<svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 100 100'><text y='.9em' font-size='90'>🐦</text></svg>"
            />
        </head>
        <body>
            <header>
                <nav>
                    <ul>
                        <li><a href="/">🏠 Accueil</a></li>
                        <li><a href="/exemples.html">🗒️ Exemples</a></li>
                        <li class="separator"><a href="/changelog.html">📋 Changelog</a></li>
                        <li><a href="https://git.kobold.cafe/kazhnuz/moineau">Sources</a></li>
                    </ul>
                </nav>
            </header>
            <main>
                <article>
                    ${marked.parse(inputContent)}
                </article>
            </main>
            <hr>
            <footer class="flex">
                <div><small>moineau</small></div>
                <div><small>CC BY <a href="https://kazhnuz.space/">Kazhnuz</a></small></div>
            </footer>
        </body>
    </html>`

    fs.writeFileSync(outFile, content)
}

parse("README.md", "dist/index.html")
parse("exemples/exemples.md", "dist/exemples.html")
parse("CHANGELOG.md", "dist/changelog.html")