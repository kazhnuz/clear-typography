# Exemples

Quelques exemples des styles que prennent différents composants

## Details

La balise `<details>` permet de cacher et afficher une information au clic. Ici, elle a été stylisé comme un bloc, et le sommaire a reçu un état au survol pour que ce soit plus visible que c'est un élément interactif

<details>
<summary>Sommaire</summary>

Contenu du `details`.

</details>

## Mark

La balise `<mark>` permet d'afficher simplement des éléments en <mark>surbrillance</mark>. Cela utilise la couleur d'accent claire (et sombre en mode sombre). Le style est proche de celui de code, mais sans monospace et avec la couleur d'accent.

# Image

Une image dans un paragraphe seul sera automatiquement en bloc, centrée et responsive

![Une photo de chat, bien sûr](/cat.jpg)

# Table

Le style des tableaux

<div class="responsive-table">
    <table>
    <caption>
        Exemple de tableau
    </caption>
    <thead>
        <tr>
            <th scope="col">Animal</th>
            <th scope="col">En liberté</th>
            <th scope="col">Nombre</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <th scope="row">Canard</th>
            <td>Oui</td>
            <td>3</td>
        </tr>
        <tr>
            <th scope="row">Poules</th>
            <td>Non</td>
            <td>4</td>
        </tr>
        <tr>
            <th scope="row">Oies</th>
            <td>Plus ou moins</td>
            <td>6</td>
        </tr>
        <tr>
            <th scope="row">Chiens</th>
            <td>Oui</td>
            <td>2</td>
        </tr>
        <tr>
            <th scope="row">Chats</th>
            <td>Oui</td>
            <td>5</td>
        </tr>
        <tr>
            <th scope="row">Perroquet (gris du gabon et deux verts)</th>
            <td>Non</td>
            <td>3</td>
        </tr>
        <tr>
            <th scope="row">Iguane</th>
            <td>Non</td>
            <td>1</td>
        </tr>
    </tbody>
    <tfoot>
        <tr>
            <th scope="row" colspan="2">Nombre total</th>
            <td>24</td>
        </tr>
    </tfoot>
    </table>
</div>

## Classes utilitaires

Quelques classes CSS sont utilisées afin d'avoir quelques outils supplémentaires utiles, notamment dans le cadre des flexboxes.

- `.flex` est utilisé pour afficher deux éléments cotes à cotes
- `.separator` est utilsié pour afficher un élément à la fin d'une navbar (cela poussera tout les composants après). Il est utilisé par exemple dans le bouton "Changelog" de la navbar pour pousser les suivants.
- `.p` est utlisé pour se faire comporter une balise comme un paragraphe, se combine bien avec .flex dans les articles
- `.responsive-table` est utilisé dans un dev pour entourer un tableau et le rendre plus ou moins responsive

## Artictecture des titres

Les différents types ont différent style, et voici ce que ça donne du h3 au h6 (le h1 est le titre de page, et le h2 celui juste au dessus)

### Titre H3

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vitae augue ut enim iaculis consequat. 

#### Titre H4

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vitae augue ut enim iaculis consequat. 

##### Titre H5

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vitae augue ut enim iaculis consequat. 

###### Titre H6

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vitae augue ut enim iaculis consequat. 

## Hgroup

Les hgroup sont supporté. Tout texte dedans sera en gras, et les titres n'auront pas leur margin-bottom habituel :

```html
<hgroup>
  <h1>Titre</h1>
  <p>Sous-titre géré dans le hgroup</p>
</hgroup>
```

donne le résultat suivant :

<hgroup>
    <h1>Titre</h1>
    <p>Sous-titre géré dans le hgroup</p>
</hgroup>

## Input

Les différents types d'input ont des styles 

<form>
    <label for="text">Champ de texte :</label>
    <input type="text" id="text" name="text" />
    <div class="p"><label for="checkbox">Champ de checkbox :</label>
    <input type="checkbox" id="checkbox" name="checkbox" /></div>
    <label for="color">Champ de couleur :</label>
    <input type="color" id="color" name="color" />
    <label for="date">Champ de date :</label>
    <input type="date" id="date" name="date" />
    <label for="time">Champ d'heure :</label>
    <input type="time" id="time" name="time" />
    <label for="datetime-local">Champ de date et heure :</label>
    <input type="datetime-local" id="datetime-local" name="datetime-local" />
    <label for="email">Champ de mail :</label>
    <input type="email" id="email" name="email" />
    <label for="tel">Champ de tel :</label>
    <input type="tel" id="tel" name="tel" />
    <label for="file">Champ de fichier :</label>
    <input type="file" id="file" name="file" />
    <label for="number">Champ de nombre :</label>
    <input type="number" id="number" name="number" />
    <div class="p"><label for="radio">Champ radio :</label>
    <input type="radio" id="radio" name="radio" /></div>
    <label for="select">Champ select:</label>
    <select name="select" id="select">
        <option value="">--Choix d'option--</option>
        <option value="canard">Canard</option>
        <option value="oie">Oie</option>
        <option value="poule">Poule</option>
        <option value="faisan">Faisant</option>
        <option value="dindon">Dindon</option>
    </select>
    <div>
        <input type="submit" id="submit" name="submit" />
        <input type="reset" id="reset" name="reset" />
    </div>

</form>
<div><textarea>uwu</textarea></div>