# Moineau

**Moineau** est un projet de feuille de style minimale inspirée des sites [bettermotherfuckingwebsite](http://bettermotherfuckingwebsite.com/) et [thebestmotherfucking.website](https://thebestmotherfucking.website/). Son but est d'être utilisé en remplacement du style par défaut de navigateur web pour des pages web "brutes", tout en restant agréable et fonctionnel.

Son but est d'avoir un style agréable à l'oeil et fonctionnel, assez minimaliste, utilisant les balises et propriété de l'HTML brut. Il n'y a pas de classe utilisé, ni de fonctionnalité de layout (à l'exception d'un support des navbar via `<nav>` qui donne des navbar horizontales).

Si le projet utilise surtout les balises standard du HTML, quelques utilitaires vont être ajoutés.

<center><a class="button" href="https://git.kobold.cafe/kazhnuz/moineau/releases">Télécharger</a></center>

## Fonctionnalités

Moineau supporte les fonctionnalités suivantes de base :

- Feuille de style simple et légère (<7 ko minifié)
- Responsive (en tout cas le plus possible)
- Concentré sur la typographie et n'utilisant pas de classes
- Gestion des couleurs
  - Support du mode sombre
  - Customisation des couleurs via des variables

## Pourquoi

Tout les sites n'ont pas besoins de design spécifique, et le but de cette feuille de style est de fournir une feuille de style simple et customisable pour des sites "tout bêtes". Il a notamment été *optimisé* pour un cas spécifique : la génération de page web à partir de page markdown, mais supporte quelques cas en plus (notamment les balises standards du HTML).

L'idée est que dans le cas de ce genre de site peuvent avoir besoin de style simple et léger, s'adapter à de différents écrans et surtout lisible : pas de lignes trop longues, et avec un texte aéré (interligne, fin de paragraphes). Le but est d'avoir un côté un peu moderne aussi, avec quelques touches de couleurs.

L'idée est de pouvoir générer simplement un blog ou un site simple pour les gens en ayant besoin. Je ne pense pas que ce sera beaucoup utilisé, mais cela peut servir dans quelques cas spécifiques ^^

## Customiser

La couleur d'accents se customise en modifiant quatre variables :

```css
:root {
  --accent-color-dark: #1971c2;
  --accent-color-light: #d0ebff;
  --accent-color-dark-hover: #1864ab;
  --accent-color-light-hover: #e7f5ff;
}
```

La première détermine la couleur d'accent sombre (donc écrite sur fond clair, et en fond sur fond sombre), et la seconde celle d'accent clair (du coup l'inverse de la sombre : écrite sur fond sombre, et en fond sur fond clair). Les deux suivantes sont les mêmes, mais avec cette fois en variante pour le survol de la souris.

Toute la palette peut également être customisé avec les variables css, de la manière suivante :

```css
:root {
  --text-color: #212529;
  --border-color: rgba(0,0,0,0.1);
  --hover-color: rgba(0,0,0,0.05);

  --background-color: #f8f9fa;
  --well-color: #e9ecef;
}

@media (prefers-color-scheme: dark) {
  :root {
    --text-color: #e9ecef;
    --background-color: #212529;
    --well-color: #343a40;

    --border-color: rgba(255,255,255,0.2);
    --hover-color: rgba(255,255,255,0.1);
  }
}
```

## Crédits

- Les couleurs proviennent de [open-color](https://yeun.github.io/open-color/)
- La photo de chat utilisé dans les exemples a été publiée sur Wikimedia.